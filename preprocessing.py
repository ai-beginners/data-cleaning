
# Preprocessing in NLP

# Step 1

# Noise Removal

import re

text = "Five fantastic fish flew off to find faraway functions. Maybe find another five fantastic fish? Find my fish with a function please!"

# remove punctuation
result = re.sub(r'[\.\?\!\,\:\;\"]', '', text)

print(result)
# Five fantastic fish flew off to find faraway functions Maybe find another five fantastic fish Find my fish with a function please




# Step 2
# Tokonization

# In natural language processing, tokenization is the text preprocessing task of breaking up text into smaller components of text (known as tokens).
from nltk.tokenize import word_tokenize

text = "This is a text to tokenize"
tokenized = word_tokenize(text)

print(tokenized)
# ["This", "is", "a", "text", "to", "tokenize"]



# Step 3

#Text Normalization
#In natural language processing, normalization encompasses many text preprocessing tasks including stemming, lemmatization, upper or lowercasing, and stopwords removal.

# Stemming

from nltk.stem import PorterStemmer

tokenized = ["So", "many", "squids", "are", "jumping"]

stemmer = PorterStemmer()
stemmed = [stemmer.stem(token) for token in tokenized]

print(stemmed)
# ['So', 'mani', 'squid', 'are', 'jump']


# Lemmatiztion

from nltk.stem import WordNetLemmatizer

tokenized = ["So", "many", "squids", "are", "jumping"]

lemmatizer = WordNetLemmatizer()
lemmatized = [lemmatizer.lemmatize(token) for token in tokenized]

print(stemmed)
# ['So', 'many', 'squid', 'be', 'jump']



# Remove Stop Words


from nltk.corpus import stopwords

# define set of English stopwords
stop_words = set(stopwords.words('english'))

# remove stopwords from tokens in dataset
statement_no_stop = [word for word in tokenized if word not in stop_words]




