import pandas as pd
from sklearn.model_selection import train_test_split
dataset=pd.read_csv("california_housing.csv")




# # to get the title and the first records
#
print(dataset.head())

# to get the column "Median_income" as Y axias
#
y=dataset.median_income


#
#
# # to remove the column
#to remove more thatn one column
# toberemovedcolumns=['median_income','language']
# x=dataset.drop(columns=toberemovedcolumns)


# To remove one column

x= dataset.drop('median_income',axis=1)

# Part 2 Splitting the Dataset usign Sklearn
# don't forget to import sklearn.model_selection

x_train,x_test,y_train,y_test=train_test_split(x,y,test_size=0.2)

# # To compare the shape of different testing and training sets, use the following piece of code:

# print("shape of original dataset",dataset.shape)
#
print("shape of input - training set :, ",x_train.shape)
print("shape of the input_ testing data ", x_test.shape)
print("shape of the out_training set",y_train.shape)
print("shape of the output testing set",y_test.shape)
